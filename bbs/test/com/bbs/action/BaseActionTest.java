package com.bbs.action;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("ServiceTest.xml")
public class BaseActionTest extends AbstractJUnit4SpringContextTests {

	
	private BaseAction baseraction;
	
	
	public BaseAction getBaseraction() {
		return baseraction;
	}

	@Autowired
	public void setBaseraction(BaseAction baseraction) {
		this.baseraction = baseraction;
	}

	@Test
	public void testLoadindex() {
		String srt = baseraction.loadindex();
		System.out.println(srt);
	}

	@Test
	public void testLoadBoardView() {

		baseraction.loadTopic();
	}

	@Test
	public void testLoadTopic() {

		baseraction.loadTopic();
	}

	@Test
	public void testLoadCommon() {
		//baseraction.loadCommon();
	}

}
