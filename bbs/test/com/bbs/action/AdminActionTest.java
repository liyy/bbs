package com.bbs.action;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbs.dto.MemberLogin;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("ServiceTest.xml")
public class AdminActionTest extends AbstractJUnit4SpringContextTests {

	@Autowired
	private AdminAction adminaction; 
	
	
	public AdminAction getAdminaction() {
		return adminaction;
	}

	public void setAdminaction(AdminAction adminaction) {
		this.adminaction = adminaction;
	}

	@Test
	public void testLogin() {
		MemberLogin login=new MemberLogin();
		login.setLoginname("ee");
		login.setLoginpwd("ee");
		adminaction.setLogin(login);
		String str = adminaction.login();
		System.out.println(str);
		
	}

	@Test
	public void testAdd() {
		fail("Not yet implemented");
	}

}
