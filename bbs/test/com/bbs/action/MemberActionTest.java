package com.bbs.action;


import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbs.dto.MemberLogin;
import com.bbs.dto.MemberRegister;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("ServiceTest.xml")
public class MemberActionTest  extends AbstractJUnit4SpringContextTests{

	@Autowired
	private MemberAction memberaction;
	@Test
	public void testLogin() {
          MemberLogin login = new MemberLogin();
		
		login.setLoginname("aa");
		login.setLoginpwd("aa");
		memberaction.setLogin(login);
	 assertEquals(true, memberaction.login());
	}

	@Test
	public void testRegister() {
		MemberRegister register = new MemberRegister();
		register.setMname("aaa");
		register.setMpassword("123456");
		register.setMpassword2("123456");
		memberaction.setRegister(register);
		memberaction.register();
	}

	@Test
	public void testTopic() {
		fail("Not yet implemented");
	}

}
