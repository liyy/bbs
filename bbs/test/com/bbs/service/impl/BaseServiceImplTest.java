package com.bbs.service.impl;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bbs.service.BaseService;

/**
 * @ClassName: BaseServiceImplTest
 * @Description: TODO(测试基础服务层的接口实现类)
 *
 */

public class BaseServiceImplTest {

	@Test
	public void testloadTemplates()
	{
		ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
	
	}
	
	@Test
	public void testLoad()
	{
		ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		BaseService bsi = (BaseService) ctx.getBean("baseservice");
		System.out.println(bsi.lastTopic());
		
	}
}
