package com.bbs.dao.impl;

import static org.junit.Assert.fail;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbs.dao.AdminDao;
import com.bbs.dto.MemberLogin;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("ServiceTest.xml")
public class AdminDaoImplTest extends AbstractJUnit4SpringContextTests {

	@Autowired
	private AdminDao admindao;
	
	public AdminDao getAdmindao() {
		return admindao;
	}

	public void setAdmindao(AdminDao admindao) {
		this.admindao = admindao;
	}

	@Test
	public void testAddMember() {
		
	}

	@Test
	public void testLoginMember() {
		MemberLogin memberlogin = new MemberLogin();
		memberlogin.setLoginname("aa");
		memberlogin.setLoginpwd("aa");
		admindao.loginMember(memberlogin);
	}

	@Test
	public void testLoadMembers() {
         admindao.loadMembers(null, null);
	}

	@Test
	public void testLoadTopics() {
		fail("Not yet implemented");
	}

}
