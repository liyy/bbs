package com.bbs.dao.impl;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbs.dao.MemberDao;
import com.bbs.dto.MemberLogin;
import com.bbs.dto.MemberRegister;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("ServiceTest.xml")
public class MemberDaoImplTest extends AbstractJUnit4SpringContextTests {

	@Autowired
	private MemberDao memberdao;
	
	@Test
	public void testExists() {
		assertEquals(true, memberdao.exists("aa"));
	}

	@Test
	public void testLoginMember() {
		MemberLogin login = new MemberLogin();
		
		login.setLoginname("aa");
		login.setLoginpwd("aa");
		assertEquals(true, memberdao.loginMember(login));
	}

	@Test
	public void testRegisterMember() {
		MemberRegister mr = new MemberRegister();
		mr.setMname("aa");
		mr.setMpassword("aa");
		mr.setMemail("test@163.com");
		assertEquals(true, memberdao.registerMember(mr));
	}

}
