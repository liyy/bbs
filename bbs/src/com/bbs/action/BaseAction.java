package com.bbs.action;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.bbs.dto.CategoryTopic;
import com.bbs.dto.LastTopic;
import com.bbs.dto.TopicReply;
import com.bbs.model.Announcements;
import com.bbs.model.Category;
import com.bbs.model.Links;
import com.bbs.model.Member;
import com.bbs.model.Navbar;
import com.bbs.model.Topic;
import com.bbs.model.TopicType;
import com.bbs.service.BaseService;
import com.bbs.service.MemberService;
import com.bbs.util.Page;
import com.opensymphony.xwork2.ActionSupport;

@Component("baseAction")
@Scope("prototype")
public class BaseAction extends ActionSupport {
	
	private BaseService baseservice;
	private MemberService memberservice;
	
	//[start]
	private Member member;  //会员
	private List<Links> links;  //友情连接
	private List<Navbar> navbars; //导航栏
	private Announcements announ;  //网站公告
	private List<Announcements> catannoun;  //版块公告
	private List<Category> categorys; //首页版块集合(所有版块) 形成树
	private Category category;  //版块信息
	private int Categoryid; //要加载的版块id
	
	private List<Topic> topics;   //帖子集合

	private List<TopicType> topictypes;  //帖子类型
	private List<CategoryTopic> catetopics; //版块下显示的每一行的帖子集合
	private List<LastTopic> lasttopics; //最后发表帖子的集合
	
	private Topic topic;    //帖子页面显示
	private List<TopicReply> topicreplys;   //帖子页面显示的评论的集合
	
	private Page page; //分页信息
	
	//private boolean topicstate = false;  //是否结贴
    //[end]
	//~~~~~~~~~~~~~~~~~~~~Action~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~/
	//访问首页时需要加载的数据
	public String loadindex()
	{
		if(baseservice==null)
		{
		System.out.println(baseservice);
		}
		//loadCommon();
		navbars = baseservice.loadNavbar();  
		links = baseservice.loadLinks();
		//announ = null;
		categorys = baseservice.loadCategory();
		lasttopics = baseservice.lastTopic();    //首页显示的各个版块最后发表的帖子
		System.out.println("index------------");
		return SUCCESS;
	}

	
	//根据传来的id加载帖子页数据
	public String loadBoardView()
	{
		category = baseservice.getCategory(1);    //根据传来的版块id 读取该版块信息
	
		catetopics = baseservice.loadCategoryTopic(1, page);  //根据传来的版块id 读取该版块下前多少条 帖子信息
		
		 // topics = null;     //根据传来的版块id 读取该版块的前多少条帖子
		//catannoun = null;  //根据传来的版块id 读取该版块的所有公告
		//topictypes = null;  //根据传来的版块id 读取该版块的所有帖子类型
		return SUCCESS;
	}
	
	//加载帖子页面的数据
	public String loadTopic()
	{
		member = baseservice.publihMember(1);         //根据帖子id  读取发帖的会员
		topic = baseservice.loadTopic(1);             //根据帖子id  读取相应的帖子
	    topicreplys = baseservice.loadTopicReply(1);  //根据帖子id  读取该帖子的所有评论
	  
		return SUCCESS;
	}
	
	
	//加载公共部分数据
	public void loadCommon()
	{
		//member = mservice.exists(member.getMname());
		navbars = baseservice.loadNavbar();  
		links = baseservice.loadLinks();
	}

	
	
	@Resource
	public void setBaseservice(BaseService baseservice) {
		this.baseservice = baseservice;
	}

	@Resource
	public void setMemberservice(MemberService memberservice) {
		this.memberservice = memberservice;
	}
	public BaseService getBaseservice() {
		return baseservice;
	}
	//~~~~~~~~~~~~~~~~~~~~~Access~~~~~~~~~~~~~~~~~~~~~~~~~~/

	public MemberService getMemberservice() {
		return memberservice;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public List<Links> getLinks() {
		return links;
	}


	public void setLinks(List<Links> links) {
		this.links = links;
	}

	public List<Navbar> getNavbars() {
		return navbars;
	}


	public void setNavbars(List<Navbar> navbars) {
		this.navbars = navbars;
	}


	public Announcements getAnnoun() {
		return announ;
	}

	public void setAnnoun(Announcements announ) {
		this.announ = announ;
	}


	public List<Category> getCategorys() {
		return categorys;
	}


	public void setCategorys(List<Category> categorys) {
		this.categorys = categorys;
	}


	public List<Topic> getTopics() {
		return topics;
	}


	public void setTopics(List<Topic> topics) {
		this.topics = topics;
	}

	public int getCategoryid() {
		return Categoryid;
	}

	public void setCategoryid(int categoryid) {
		Categoryid = categoryid;
	}

	public List<TopicType> getTopictypes() {
		return topictypes;
	}


	public void setTopictypes(List<TopicType> topictypes) {
		this.topictypes = topictypes;
	}
	
	public List<Announcements> getCatannoun() {
		return catannoun;
	}


	public void setCatannoun(List<Announcements> catannoun) {
		this.catannoun = catannoun;
	}
	
	public Category getCategory() {
		return category;
	}


	public void setCategory(Category category) {
		this.category = category;
	}

	public List<CategoryTopic> getCatetopics() {
		return catetopics;
	}


	public void setCatetopics(List<CategoryTopic> catetopics) {
		this.catetopics = catetopics;
	}



	public List<LastTopic> getLasttopics() {
		return lasttopics;
	}


	public void setLasttopics(List<LastTopic> lasttopics) {
		this.lasttopics = lasttopics;
	}


	public Topic getTopic() {
		return topic;
	}


	public void setTopic(Topic topic) {
		this.topic = topic;
	}


	public List<TopicReply> getTopicreplys() {
		return topicreplys;
	}


	public void setTopicreplys(List<TopicReply> topicreplys) {
		this.topicreplys = topicreplys;
	}


	public Page getPage() {
		return page;
	}


	public void setPage(Page page) {
		this.page = page;
	}




	
}