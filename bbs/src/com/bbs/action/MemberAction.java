package com.bbs.action;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.bbs.dto.MemberLogin;
import com.bbs.dto.MemberRegister;
import com.bbs.model.Topic;
import com.bbs.service.MemberService;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@Component("memberAction")
@Scope("prototype")
public class MemberAction extends ActionSupport implements ModelDriven<MemberRegister>, SessionAware {
	
	private MemberService mservice;
	
	private MemberRegister register = new MemberRegister();
	
	private MemberLogin login=null;
	private Map<String,Object> memberResult = new HashMap<String,Object>();
	private Map<String,Object> session;
	
	private Topic topic;
	/** 编辑器 */
	private String FckEditor;
	        //~~~~~~~~~~~~~~~~~~~~~Action~~~~~~~~~~~~~~~~~~~~~~~~~~~~~/
	/**
	 * 会员登录
	 */
	public String login()
	{
		if(mservice.loginMember(login))
		{
			memberResult.put("result", true);
			memberResult.put("msg", "登录成功！");
			memberResult.put("url", "index.jsp");
			session.put("username", login.getLoginname());
			
		}
		else
		{
			memberResult.put("result", false);
			memberResult.put("msg", "登录失败！请联系管理员");
			memberResult.put("url", "common/login.jsp");
		}
		System.out.println("login "+login.getLoginname());
		System.out.println("pwd "+login.getLoginpwd());
		return SUCCESS;
	}
	
	public String register()
	{
		if(mservice.registerMember(register))
		{
			memberResult.put("result", true);
			memberResult.put("msg", "注册成功！");
			memberResult.put("url", "common/mode.jsp");
			
		}
		else
		{
			memberResult.put("result", false);
			memberResult.put("msg", "注册失败！");
			memberResult.put("url", "common/register.jsp");
		}
		return SUCCESS;
	}
	
	/**
   	 * 发帖
	 */
	public String topic()
	{
		String name  = (String) session.get("username");
		if(mservice.exists(name)!=null)
		{
			String ip =this.getIpAddr();
			topic.setTuserip(ip);
			if(mservice.addtopic(topic))
			{
				memberResult.put("result", true);
				memberResult.put("msg", "发帖成功！");
				memberResult.put("url", "common/topic.jsp");
			    
			}
			else{
				memberResult.put("result", false);
				memberResult.put("msg", "发帖失败！请联系网站管理员");
				memberResult.put("url", "common/post.jsp");
			}
		}
		else
		{
			memberResult.put("result", false);
			memberResult.put("msg", "发帖失败！不存在该用户,请联系网站管理员");
			memberResult.put("url", "index.jsp");
		}
		return SUCCESS;
	}

	/**
     * 获得客户端真是ip
     */
    public  String getIpAddr() {    
    	HttpServletRequest request = ServletActionContext. getRequest(); 
        String ip = request.getHeader("x-forwarded-for");    
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {    
            ip = request.getHeader("Proxy-Client-IP");    
        }    
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {    
            ip = request.getHeader("WL-Proxy-Client-IP");    
        }    
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {    
            ip = request.getRemoteAddr();    
        }
		return ip;    
    }
	
    //~~~~~~~~~~~~~~~~~~~~~~Access Date~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~/
	public void setSession(Map<String, Object> session) {
		this.session =  session;
		
	}
	

	public MemberRegister getModel() {
		return register;
	}
	
	

                   
	public MemberRegister getRegister() {
		return register;
	}

	public void setRegister(MemberRegister register) {
		this.register = register;
	}

	public MemberService getMservice() {
		return mservice;
	}

	@Resource
	public void setMservice(MemberService mservice) {
		this.mservice = mservice;
	}

	
	public MemberLogin getLogin() {
		return login;
	}

	public void setLogin(MemberLogin login) {
		this.login = login;
	}

	public String getFckEditor() {
		return FckEditor;
	}

	public void setFckEditor(String fckEditor) {
		FckEditor = fckEditor;
	}

	public Map<String, Object> getMemberResult() {
		return memberResult;
	}

	public void setMemberResult(Map<String, Object> memberResult) {
		this.memberResult = memberResult;
	}

	public Topic getTopic() {
		return topic;
	}

	public void setTopic(Topic topic) {
		this.topic = topic;
	}

	

	
}
