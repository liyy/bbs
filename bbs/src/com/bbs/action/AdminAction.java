package com.bbs.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.bbs.dto.MemberLogin;
import com.bbs.dto.MemberRegister;
import com.bbs.dto.SearchBean;
import com.bbs.model.Topic;
import com.bbs.service.AdminService;
import com.bbs.util.Page;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@Component("adminAction")
@Scope("prototype")
public class AdminAction extends ActionSupport implements ModelDriven<MemberLogin> , SessionAware  {

	private AdminService aservice;
	private MemberLogin login=new MemberLogin();
	private MemberRegister register;
	private Map<String, Object> session;
	private Map<String,Object> loginResult = new HashMap<String,Object>();
	private List<Topic> topics;
	private Page page; //分页信息
	private SearchBean search;
	
	// ~~~~~~~~~~~~~~~~~~~~~~~  Action Methods ~~~~~~~~~~~~~~~~~~~~~~~~~~//  
	public String login()
	{
		if(aservice.loginMember(login))
		{
			loginResult.put("success", true);
			session.put("username", login.getLoginname());
		    return "ajaxlogin";
		}
		loginResult.put("success", false);
		return INPUT;
	}
	
	public String add()
	{
		if(aservice.addMember(register))
		{
			return SUCCESS;
		}
		return INPUT;
	}
	
	public String loadtopic()
	{
		
		topics =  aservice.loadTopics(search, page);
		return SUCCESS;
	}
	
	public String index()
	{
		return SUCCESS;
	}
	
	
	public void setSession(Map<String, Object> session) {
		
		this.session = session; 
	}

	public MemberLogin getModel() {
		return login;
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~Accessor Methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	public AdminService getAservice() {
		return aservice;
	}

	@Resource
	public void setAservice(AdminService aservice) {
		this.aservice = aservice;
	}

	public MemberLogin getLogin() {
		return login;
	}

	public void setLogin(MemberLogin login) {
		this.login = login;
	}

	public Map<String, Object> getLoginResult() {
		return loginResult;
	}

	public void setLoginResult(Map<String, Object> loginResult) {
		this.loginResult = loginResult;
	}

	public MemberRegister getRegister() {
		return register;
	}

	public void setRegister(MemberRegister register) {
		this.register = register;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public List<Topic> getTopics() {
		return topics;
	}

	public void setTopics(List<Topic> topics) {
		this.topics = topics;
	}

	public SearchBean getSearch() {
		return search;
	}

	public void setSearch(SearchBean search) {
		this.search = search;
	}
	
	
	
}
