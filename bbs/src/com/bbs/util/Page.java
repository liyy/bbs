package com.bbs.util;

/**
* 分页
*
 */
public class Page {

	private static final long serialVersionUID = -7742988063989166270L;
	
	/** 起始页数 */
	private int firstPage;
    
	/** 结束页数 */
    private int lastPage;
       
    /** next page */
    private int nextPage;
    
    /** prev page*/
    private int prevPage;
       
    /** 当前页 */
    private int currentPage;
    
    /** 总页数 */
    private int totalPage;
    
    /** 总条数 */
    private int rowCount;
    
    /** 每页显示数 */
    private int pageSize =20;
    
    /** exists next page */
    private boolean hasNext;
    
    /** exists prev page */
    private boolean hasPrev;
    
    /** exists first page */
    private boolean hasFirst;
    
    /** exists last page */
    private boolean hasLast;
    
    /** Constructor default */
    public Page() {}

    /** call this constructor method get instance */
    public Page(int rowCount,int pageSize,int currentPage) {
    	this.rowCount = rowCount;   
        this.currentPage = currentPage;   
        this.pageSize = pageSize;
        this.totalPage = this.rowCount % pageSize == 0 ? this.rowCount / pageSize : this.rowCount / pageSize + 1 ;      
        if(this.totalPage > 0){   
            this.hasFirst = true ;   
            this.firstPage = 1 ;   
        }   
        if(this.currentPage  > 1 ){   
            this.hasPrev = true ;   
            this.prevPage = this.currentPage - 1;   
        }
        if(this.totalPage > 0 && this.currentPage < this.totalPage){   
            this.hasNext = true;   
            this.nextPage = this.currentPage + 1 ;   
        }   
        if(this.totalPage > 0){   
            this.hasLast = true;   
            this.lastPage = this.totalPage;   
        }
    }

    //[start] get set
	public int getFirstPage() {
		return firstPage;
	}

	public void setFirstPage(int firstPage) {
		this.firstPage = firstPage;
	}

	public int getLastPage() {
		return lastPage;
	}

	public void setLastPage(int lastPage) {
		this.lastPage = lastPage;
	}

	public int getNextPage() {
		return nextPage;
	}

	public void setNextPage(int nextPage) {
		this.nextPage = nextPage;
	}

	public int getPrevPage() {
		return prevPage;
	}

	public void setPrevPage(int prevPage) {
		this.prevPage = prevPage;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public boolean isHasNext() {
		return hasNext;
	}

	public void setHasNext(boolean hasNext) {
		this.hasNext = hasNext;
	}

	public boolean isHasPrev() {
		return hasPrev;
	}

	public void setHasPrev(boolean hasPrev) {
		this.hasPrev = hasPrev;
	}

	public boolean isHasFirst() {
		return hasFirst;
	}

	public void setHasFirst(boolean hasFirst) {
		this.hasFirst = hasFirst;
	}

	public boolean isHasLast() {
		return hasLast;
	}

	public void setHasLast(boolean hasLast) {
		this.hasLast = hasLast;
	}
    //[end]
    
}
