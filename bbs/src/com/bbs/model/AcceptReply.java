package com.bbs.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * @ClassName: AcceptReply
 * 评论采纳实体
 *
 */

@Entity
public class AcceptReply {

	private int acceptid;
	//private int topicid;      //帖子id
	private Topic topicid;
	//private int replyid;      //评论id
	private Reply replyid;
	private int rscore;       //该评论所得分数（大于0）
	
	@Id
	@GeneratedValue
	public int getAcceptid() {
		return acceptid;
	}


	/**
	 *AcceptReply外键topicid 帖子id
	 *多对一
	 **/
	@OneToOne
	@JoinColumn(name="topicid")
	public Topic getTopicid() {
		return topicid;
	}
	
	/**
	 * AcceptReply外键replyid 评论id
	 * 多对一
	 **/
	@ManyToOne
	@JoinColumn(name="replyid")
	public Reply getReplyid() {
		return replyid;
	}
	
	public void setAcceptid(int acceptid) {
		this.acceptid = acceptid;
	}
	
	
	public void setReplyid(Reply replyid) {
		this.replyid = replyid;
	}
	
	public void setTopicid(Topic topicid) {
		this.topicid = topicid;
	}
	
	public int getRscore() {
		return rscore;
	}
	
	

	public void setRscore(int rscore) {
		this.rscore = rscore;
	}
	
	
}
