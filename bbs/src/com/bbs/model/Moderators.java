package com.bbs.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * 每个版块对应的版主实体
 */
@Entity
public class Moderators {
	
	private int moderatorsid ;
	//private int mid;      //对应的会员id
	private Member mid; 
	
	//private int categoryid; //对应的版块
	private Category categoryid;

	@Id
	@GeneratedValue
	public int getModeratorsid() {
		return moderatorsid;
	}

	public void setModeratorsid(int moderatorsid) {
		this.moderatorsid = moderatorsid;
	}

	/**
	 * 版主对应的会员id  多对一   
	 * 一个会员可以是多个版块的版主
	 */
	@ManyToOne
	@JoinColumn(name="mid")
	public Member getMid() {
		return mid;
	}

	public void setMid(Member mid) {
		this.mid = mid;
	}

	/**
	 * 对应的版块id  多对一   
	 * 每个版块可以有多个版主
	 */
	@ManyToOne
	@JoinColumn(name="categoryid")
	public Category getCategoryid() {
		return categoryid;
	}

	public void setCategoryid(Category categoryid) {
		this.categoryid = categoryid;
	} 
	
	
}
