package com.bbs.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * 用户角色实体类
 */
@Entity
public class UserRoles {
	
	private int rolesid;
	private String rolename; //权限名称
	private int rolepermission;  //角色所具有的权限   数值越大权限越高
	private String rolepcontent; //权限说明
	
	@Id
	@GeneratedValue
	public int getRolesid() {
		return rolesid;
	}
	public void setRolesid(int rolesid) {
		this.rolesid = rolesid;
	}
	public String getRolename() {
		return rolename;
	}

	
	public int getRolepermission() {
		return rolepermission;
	}
	public void setRolepermission(int rolepermission) {
		this.rolepermission = rolepermission;
	}
	public String getRolepcontent() {
		return rolepcontent;
	}
	public void setRolename(String rolename) {
		this.rolename = rolename;
	}
	public void setRolepcontent(String rolepcontent) {
		this.rolepcontent = rolepcontent;
	}
	
}
