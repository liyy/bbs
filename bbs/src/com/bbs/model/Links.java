package com.bbs.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * 友情链接实体类
 */

@Entity
public class Links {

	private int linkid;

	private String linktitle;
	private String linkurl;  //链接的地址
	private String linkimg; //链接的图片
	
	@Id
	@GeneratedValue
	public int getLinkid() {
		return linkid;
	}
	public void setLinkid(int linkid) {
		this.linkid = linkid;
	}
	public String getLinktitle() {
		return linktitle;
	}
	public void setLinktitle(String linktitle) {
		this.linktitle = linktitle;
	}
	public String getLinkurl() {
		return linkurl;
	}
	public void setLinkurl(String linkurl) {
		this.linkurl = linkurl;
	}
	public String getLinkimg() {
		return linkimg;
	}
	public void setLinkimg(String linkimg) {
		this.linkimg = linkimg;
	}
}
