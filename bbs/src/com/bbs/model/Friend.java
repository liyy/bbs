package com.bbs.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * 每个会员好友的对应实体类
 */

@Entity
public class Friend {

	private int friendid;
	//private int mid;         //会员id
	private Member mid;   
	
	//private int bymemberid;  //被加为好友的id
	private Member bymemberid;
	
	@Id
	@GeneratedValue
	public int getFriendid() {
		return friendid;
	}
	public void setFriendid(int friendid) {
		this.friendid = friendid;
	}
	
	/**
	* 有好友的会员   mid  多对一
	 */
	@ManyToOne
	@JoinColumn(name="mid")
	public Member getMid() {
		return mid;
	}
	public void setMid(Member mid) {
		this.mid = mid;
	}
	
	/**
	* 每个会员的好友  bymemberid 多对一
	 */
	@ManyToOne
	@JoinColumn(name="bymemberid")
	public Member getBymemberid() {
		return bymemberid;
	}
	public void setBymemberid(Member bymemberid) {
		this.bymemberid = bymemberid;
	}

	
}
