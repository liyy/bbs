package com.bbs.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * 模块实体
 *
 */
@Entity
public class Category {

	private int categoryid;
	
	/**属于版块*/
	private Category fcategoryid; 
	
	
	private String ctitle;       //模板名称
	private String cicon;        //模板图标
	private String cdescription; //模板简介
	private boolean cstatus;     //是否在论坛上显示
	private int clasttopicid;    //最后发表的帖子
	
	
	
	@Id
	@GeneratedValue
	public int getCategoryid() {
		return categoryid;
	}
	

	/**
	 *  属于那个版块    多对一 
   	 * @return Category   
	 */
	@ManyToOne
	@JoinColumn(name="fcategoryid")
    public Category getFcategoryid() {
		return fcategoryid;
	}
	


	public void setCategoryid(int categoryid) {
		this.categoryid = categoryid;
	}
	public void setFcategoryid(Category fcategoryid) {
		this.fcategoryid = fcategoryid;
	}
	

	public String getCtitle() {
		return ctitle;
	}
	public void setCtitle(String ctitle) {
		this.ctitle = ctitle;
	}
	public String getCicon() {
		return cicon;
	}
	public void setCicon(String cicon) {
		this.cicon = cicon;
	}
	public String getCdescription() {
		return cdescription;
	}
	public void setCdescription(String cdescription) {
		this.cdescription = cdescription;
	}
	public boolean isCstatus() {
		return cstatus;
	}
	public void setCstatus(boolean cstatus) {
		this.cstatus = cstatus;
	}


	public int getClasttopicid() {
		return clasttopicid;
	}


	public void setClasttopicid(int clasttopicid) {
		this.clasttopicid = clasttopicid;
	}

	
	
}
