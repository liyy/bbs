package com.bbs.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * 帖子实体类
 */
@Entity
public class Topic {

	private int topicid;
	//private int typeid; //所属类型id
	private TopicType typeid; 
	
	//private int categoryid; //所属模块id
	private Category categoryid; //所属模块id
	
	//private int tauthorid;      //发帖会员id
	private Member tauthorid;
	
	private String topictitle; //帖子标题
	private String tcontent;  //帖子内容
	private String tuserip;  //发表者的IP
	private int tfraciton;  //发帖人规定的分数
	//private boolean topicstate;  //是否结贴（提问）  在采纳表中有该贴的采纳时默认结贴
	private Date tdateline;    //帖子的发布时间
	private boolean tstickie;  //是否置顶 （管理员设置）
	private boolean topicstate;     //帖子状态是否可以查看（管理员设置）
	private boolean tdraught;   //是否是草稿(自己设置)
	
	@Id
	@GeneratedValue
	public int getTopicid() {
		return topicid;
	}

	/**
	 * 发帖的会员id 多对一   
	 * 一位会员可以发多条贴子
	 */
	@ManyToOne
	@JoinColumn(name="tauthorid")
	public Member getTauthorid() {
		return tauthorid;
	}
	

	/**
	 * 该帖子的类型id  多对一
	 *
	 */
	@ManyToOne
	@JoinColumn(name="typeid")
	public TopicType getTypeid() {
		return typeid;
	}

	/**
	 * 该帖子属于那个版块    多对一
	* @return Category    
	 */
	@ManyToOne
	@JoinColumn(name="categoryid")
	public Category getCategoryid() {
		return categoryid;
	}

	public void setTopicid(int topicid) {
		this.topicid = topicid;
	}
	public void setTauthorid(Member tauthorid) {
		this.tauthorid = tauthorid;
	}
	
	public void setCategoryid(Category categoryid) {
		this.categoryid = categoryid;
	}

	public void setTypeid(TopicType typeid) {
		this.typeid = typeid;
	}

	
	public String getTopictitle() {
		return topictitle;
	}
	public void setTopictitle(String topictitle) {
		this.topictitle = topictitle;
	}
	public String getTcontent() {
		return tcontent;
	}
	public void setTcontent(String tcontent) {
		this.tcontent = tcontent;
	}
	public String getTuserip() {
		return tuserip;
	}
	public void setTuserip(String tuserip) {
		this.tuserip = tuserip;
	}
	public int getTfraciton() {
		return tfraciton;
	}
	public void setTfraciton(int tfraciton) {
		this.tfraciton = tfraciton;
	}

	public Date getTdateline() {
		return tdateline;
	}
	public void setTdateline(Date tdateline) {
		this.tdateline = tdateline;
	}
	
	public boolean isTstickie() {
		return tstickie;
	}

	public void setTstickie(boolean tstickie) {
		this.tstickie = tstickie;
	}

	public boolean isTopicstate() {
		return topicstate;
	}

	public void setTopicstate(boolean topicstate) {
		this.topicstate = topicstate;
	}

	public boolean isTdraught() {
		return tdraught;
	}


	public void setTdraught(boolean tdraught) {
		this.tdraught = tdraught;
	}



}
