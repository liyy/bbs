package com.bbs.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * 大模块实体类
 */
@Entity
public class Templates {
	
	private int templateid;
	private String tempname;
	private String tempnotes;  //模块介绍
	private Date tcreatetime;  //创建时间
	
	@Id
	@GeneratedValue
	public int getTemplateid() {
		return templateid;
	}
	public void setTemplateid(int templateid) {
		this.templateid = templateid;
	}
	public String getTempname() {
		return tempname;
	}
	public void setTempname(String tempname) {
		this.tempname = tempname;
	}
	public String getTempnotes() {
		return tempnotes;
	}
	public void setTempnotes(String tempnotes) {
		this.tempnotes = tempnotes;
	}
	public Date getTcreatetime() {
		return tcreatetime;
	}
	public void setTcreatetime(Date tcreatetime) {
		this.tcreatetime = tcreatetime;
	}

}
