package com.bbs.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * 首页导航条实体类
 */
@Entity
public class Navbar {

	private int navid;
	private String navtitle;
	private String navurl;  //连接路径
	
	@Id
	@GeneratedValue
	public int getNavid() {
		return navid;
	}
	public void setNavid(int navid) {
		this.navid = navid;
	}
	public String getNavtitle() {
		return navtitle;
	}
	public void setNavtitle(String navtitle) {
		this.navtitle = navtitle;
	}
	public String getNavurl() {
		return navurl;
	}
	public void setNavurl(String navurl) {
		this.navurl = navurl;
	}
	
}
