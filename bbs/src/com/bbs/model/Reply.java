package com.bbs.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * 帖子回复信息实体类
 */
@Entity 
public class Reply {

	private int replyid;
	//private int topicid;
    private Topic topicid;   //回复的帖子id
	//private int postid;
	private String rcontent;  //回复的内容
	private Date rcreatetime; //回复的时间
	//private int rfrid ; // 引用的评论的id
	private Reply rfrid; 
	private Member mid;  //评论的会员id



	@Id
	@GeneratedValue
	public int getReplyid() {
		return replyid;
	}
	/**
	 * 评论所回复帖子的id   多对一
	 * 一个帖子 
	 */
	@ManyToOne
	@JoinColumn(name="topicid")
	public Topic getTopicid() {
		return topicid;
	}
	
	/**
	 * 所引用其它的回复id  一对一
	 * 一条评论只引用相同帖子的一条回复
	 */
	@ManyToOne
	@JoinColumn(name="rfrid")
	public Reply getRfrid() {
		return rfrid;
	}
	
	/**
	 * 评论的会员id  多对一
	 * @return Member 
	 */
	@ManyToOne
	@JoinColumn(name="mid")
	public Member getMid() {
		return mid;
	}
	public void setMid(Member mid) {
		this.mid = mid;
	}
	public void setRfrid(Reply rfrid) {
		this.rfrid = rfrid;
	}
	
	public void setTopicid(Topic topicid) {
		this.topicid = topicid;
	}
	
	public String getRcontent() {
		return rcontent;
	}
	public Date getRcreatetime() {
		return rcreatetime;
	}
	public void setRcontent(String rcontent) {
		this.rcontent = rcontent;
	}
	public void setRcreatetime(Date rcreatetime) {
		this.rcreatetime = rcreatetime;
	}
	public void setReplyid(int replyid) {
		this.replyid = replyid;
	}
	
	
}
