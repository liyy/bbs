package com.bbs.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
/**
 * @Description: TODO(公告实体)
 *
 */
@Entity
public class Announcements {
	
	private int amid;
//	private int mid; //公告发布者
	private Member mid;
	private String title;  //公告标题
	private Date starttime; //起始时间
	private Date endtime; //结束时间
	private String message; //公告内容
	
	private Category categoryid; //该公告属于那个版块
	
	@Id
	@GeneratedValue
	public int getAmid() {
		return amid;
	}
	public void setAmid(int amid) {
		this.amid = amid;
	}

	/**
	 * Announcements 外键 mid 公告发布者
	 *多对一关系
	 */
	@ManyToOne
	@JoinColumn(name="mid")
	public Member getMid() {
		return mid;
	}
	
	
	/**
	 * 该公告属于那个版块 多对一
	 * @return Category 
	 */
	@ManyToOne
	@JoinColumn(name="categoryid")
	public Category getCategoryid() {
		return categoryid;
	}
	public void setCategoryid(Category categoryid) {
		this.categoryid = categoryid;
	}
	public void setMid(Member mid) {
		this.mid = mid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getStarttime() {
		return starttime;
	}
	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}
	public Date getEndtime() {
		return endtime;
	}
	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
