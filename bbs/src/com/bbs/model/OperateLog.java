package com.bbs.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * 会员操作日志实体类
 */
@Entity
public class OperateLog {

	private int logid;
	//private int mid;           //操作的管理员id
	private Member mid;    
	private String lmsg;       //操作的描述
	private Date operatetime;  //操作的时间
	
	@Id
	@GeneratedValue
	public int getLogid() {
		return logid;
	}
	public void setLogid(int logid) {
		this.logid = logid;
	}

	/**
	 * 进行操作的管理员id  多对一   
	 * 一次操作对应一条记录
	 */
	@ManyToOne
	@JoinColumn(name="mid")
	public Member getMid() {
		return mid;
	}
	public void setMid(Member mid) {
		this.mid = mid;
	}
	public String getLmsg() {
		return lmsg;
	}
	public void setLmsg(String lmsg) {
		this.lmsg = lmsg;
	}
	public Date getOperatetime() {
		return operatetime;
	}
	public void setOperatetime(Date operatetime) {
		this.operatetime = operatetime;
	}
}
