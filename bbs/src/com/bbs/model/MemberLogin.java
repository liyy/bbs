package com.bbs.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * 用户登录记录
 */

@Entity
public class MemberLogin {

	private int loginid;
	//private int mid;   //登录的会员
	private Member mid;  
	private String logip; //登录的ip
	private Date dateline; //登录时间
	
	@Id
	@GeneratedValue
	public int getLoginid() {
		return loginid;
	}
	public void setLoginid(int loginid) {
		this.loginid = loginid;
	}
	
	/**
	*  每次登录的管理员  mid  多对一
	 */
	@ManyToOne
	@JoinColumn(name="mid")
	public Member getMid() {
		return mid;
	}
	public void setMid(Member mid) {
		this.mid = mid;
	}
	public String getLogip() {
		return logip;
	}
	public void setLogip(String logip) {
		this.logip = logip;
	}
	public Date getDateline() {
		return dateline;
	}
	public void setDateline(Date dateline) {
		this.dateline = dateline;
	}
	
}
