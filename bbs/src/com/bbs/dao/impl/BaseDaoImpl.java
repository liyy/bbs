package com.bbs.dao.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.bbs.dao.BaseDao;
import com.bbs.dto.CategoryTopic;
import com.bbs.dto.LastTopic;
import com.bbs.dto.TopicReply;
import com.bbs.model.AcceptReply;
import com.bbs.model.Announcements;
import com.bbs.model.Category;
import com.bbs.model.Links;
import com.bbs.model.Member;
import com.bbs.model.Navbar;
import com.bbs.model.Reply;
import com.bbs.model.Topic;
import com.bbs.util.Page;

@Component("basedao")
public class BaseDaoImpl extends HibernateDAO implements BaseDao {

	// ~~~~~~~~~~~~~index~~~~~~~~~~~~~~~~~~~//
	// public List<Templates> loadTemplates() {
	//
	// return this.getHibernateTemplate().loadAll(Templates.class);
	// //return hibernateTemplate.loadAll(Templates.class);
	// }

	public List<Navbar> loadNavbar() {

		return this.getHibernateTemplate().loadAll(Navbar.class);
	}

	public List<Links> loadLinks() {

		// return hibernateTemplate.loadAll(Links.class);
		return this.getHibernateTemplate().loadAll(Links.class);
	}

	public List<Category> loadCategory() {
		// return hibernateTemplate.loadAll(Category.class);
		return this.getHibernateTemplate().loadAll(Category.class);
		//return(List<Category>) this.getHibernateTemplate().find("from Category as s");
	}

	public List<LastTopic> lastTopic() {

		String hql1 = "select c.clasttopicid  from Category as c";
		String hql = "select ca.categoryid, to.topicid,to.topictitle,m.mname,to.tdateline from Topic as to inner join to.tauthorid as m inner join to.categoryid as ca  where "
				+ " to.topicid in(select c.clasttopicid  from Category as c)";

		return this.getHibernateTemplate().find(hql);
	}

	// ~~~~~~~~~~~~~~Category~~~~~~~~~~~~~~~~~~~//
	public Category getCategory(int Categoryid) {

		return this.getHibernateTemplate().get(Category.class, Categoryid);
	}

	public List<Announcements> loadAnnouncements(int categoryid) {

		return this.getHibernateTemplate()
				.find("from  Announcements as ann where ann.categoryid=?",
						categoryid);
	}

	public List<CategoryTopic> loadCategoryTopic(int categoryid, Page page) {
		String count = "select count(t) from Topic as t";
		int rowCount = Integer.parseInt(this.getHibernateTemplate().find(count)
				.get(0).toString());

		if (page == null) {
			page = new Page(rowCount, 10, 1);
		} else {
			page.setRowCount(rowCount);
		}
		String hql = "select new List(tt.typeid, tt.typecontent, top.topicid,top.topictitle,"
				+ " top.tauthorid, m.mname ) from Topic as top inner join TopicType as tt "
				+ " on top.typeid = tt.typeid inner join Member as m on top.tauthorid = m.mid where top.categoryid ="+categoryid+"";

		//List<CategoryTopic> list = this.getHibernateTemplate().find(hql,categoryid);
		List<CategoryTopic> list = this.getListForPage(hql, page.getCurrentPage()*10, 10);
		// List<Reply> listreply = this.getHibernateTemplate().find(sql2,
		// categoryid);

		return list;
	}

	// ~~~~~~~~~~~~~~~~Topic~~~~~~~~~~~~~~~~~~~~~~//
	public Topic loadTopic(int topicid) {

		// hql ="from Topic as t where t.topicid=?";
		return (Topic) this.loadByKey(Topic.class, "topicid", topicid);

	}

	public List<TopicReply> loadTopicReply(int topicid) {
		// TODO 未完成

		// Topic t = new Topic();
		// Reply top = new Reply();
		// top.setTopicid(t);
		// top.getTopicid().setTopicid(topicid);
		String hql1 = "select m, r from Reply as r join r.mid as m  where r.topicid=1";
		List lis1 = this.getHibernateTemplate().find(hql1);

		String hql2 = "select a from AcceptReply as a where a.topicid=1";
		List lis2 = this.getHibernateTemplate().find(hql2);

		List<TopicReply> ltr = null;
		TopicReply tr = null;
		int r = 0;
		if (lis1 != null && lis1.size() > 0) {
			for (int i = 0; i < lis1.size(); i++) {
				tr = new TopicReply();
				Object[] obj = (Object[]) lis1.get(i);
				Member m = (Member) obj[0];
				Reply re = (Reply) obj[1];
				if (lis2 != null && lis2.size() > 0) {
					AcceptReply ar = (AcceptReply) lis2.get(r);
					if (ar.getTopicid() == re.getTopicid()) {
						r++;
						tr.setAccept(true);
						tr.setRscore(ar.getAcceptid());
					}
				}
				tr.setMember(m);
				tr.setReply(re);
			}
		}
		return ltr;

		// return
		// this.getHibernateTemplate().find("from Reply as t where t.topicid=?",
		// topicid);
	}

	public Member publihMember(int topicid) {

		String hql = "select m from Topic as t inner join t.tauthorid as m where t.topicid=?";
		return (Member) this.loadKey(hql, topicid);

	}

}
