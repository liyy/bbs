package com.bbs.dao.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.bbs.dao.MemberDao;
import com.bbs.dto.MemberLogin;
import com.bbs.dto.MemberRegister;
import com.bbs.model.Member;
import com.bbs.model.Topic;

@Component("memberdao")
public class MemberDaoImpl extends HibernateDAO implements MemberDao {

	public Member exists(String name) {

		String hql = "from Member as m where m.mname=?";
		Member ob = (Member)this.loadKey(hql, name);
		if(ob!=null)
		{
			return ob;
		}
		
		return ob;
	}

	public boolean loginMember(MemberLogin memberlogin) {
		String hql = "select m.mid from Member as m where m.mname=? and m.mpassword=?";
		// Member member = (Member) this.loadKey(hql,memberlogin.getLoginname(),
		// memberlogin.getLoginpwd());
		List list = this.getHibernateTemplate().find(hql, memberlogin.getLoginname(), memberlogin.getLoginpwd());
		if (list != null && list.size() > 0) {
			Object ob = list.get(0);
			if (ob != null) {
				return true;
			}
		}

		return false;
	}

	public boolean registerMember(MemberRegister member) {

		Member m = new Member(member);
		return this.create(m);
	}

	public boolean addtopic(Topic topic) {
		// TODO Auto-generated method stub
		return false;
	}

	
}
