package com.bbs.dao;

import java.util.List;

import com.bbs.dto.CategoryTopic;
import com.bbs.dto.LastTopic;
import com.bbs.dto.TopicReply;
import com.bbs.model.Announcements;
import com.bbs.model.Category;
import com.bbs.model.Links;
import com.bbs.model.Member;
import com.bbs.model.Navbar;
import com.bbs.model.Templates;
import com.bbs.model.Topic;
import com.bbs.util.Page;

public interface BaseDao {

	
	/**
	* @Title: loadNavbar
	* 读取导航栏
	* @return List Navbar   
	 */
	List<Navbar> loadNavbar(); //读取导航栏
	
	/**
	* @Title: loadLinks
	* 读取友情链接
	* @return List Links
	 */
	List<Links> loadLinks();
	
	/**
	* @Title: loadCategory
	* 读取所有版块
	* @return List<Category>    
	 */
	List<Category> loadCategory();
	
	/**
	 * 读取首页显示的最后发表的帖子 集合
	* @return List Topic
	 */
	List<LastTopic> lastTopic();
	
	//public List<LastTopic> lastTopic(); //所有版块读取最后发表的帖子 集合
	
	/**
	 *根据id 查询一个版块的信息 
	 *@return Category
	 */
    Category getCategory(int Categoryid); 
    
    /**
     * 根据版块id  查询该板块的公告
    * @return List<Announcements>
     */
    List<Announcements> loadAnnouncements(int categoryid);
    /**
     * 根据的版块id 读取该版块下前多少条 帖子信息
    * @return List<CategoryTopic>    返回类型
     */
    List<CategoryTopic> loadCategoryTopic(int categoryid,Page page);
    
    /**
     * 根据帖子id  读取发帖的作者
    * @return Member 
     */
    Member publihMember(int topicid);
    
    /**
     * 根据帖子id  读取该帖子的评论集合
    * @return List<TopicReply>  
     */
    List<TopicReply> loadTopicReply(int topicid);
    
    /**
     * 根据帖子id  读取相应的帖子
     * @return Topic
     */
    Topic loadTopic(int topicid);
}
