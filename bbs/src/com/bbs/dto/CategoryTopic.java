package com.bbs.dto;

import java.util.Date;

import com.bbs.util.Page;

/**
 * @ClassName: LastCategory
 * 每个版块显示帖子信息的封装  
 */
public class CategoryTopic {
	
	private int typeid;             //帖子类型id
	private String typecontent;     //帖子类型
	
	private int topicid;            //帖子id
	private  String topictitle;     //帖子标题
	
	private int mid;                //作者id
	private String mname;           //作者name
	
	private int lastreplyid;        //最后评论id
	private String lastreplyauthor; //最后评论的作者
	private Date lastreplytime;     //最后评论的时间
	//private Page page;              //分页数据
	
	public int getTypeid() {
		return typeid;
	}
	public void setTypeid(int typeid) {
		this.typeid = typeid;
	}
	public String getTypecontent() {
		return typecontent;
	}
	public void setTypecontent(String typecontent) {
		this.typecontent = typecontent;
	}
	
	public int getTopicid() {
		return topicid;
	}
	public void setTopicid(int topicid) {
		this.topicid = topicid;
	}
	public String getTopictitle() {
		return topictitle;
	}
	public void setTopictitle(String topictitle) {
		this.topictitle = topictitle;
	}
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public String getMname() {
		return mname;
	}
	public void setMname(String mname) {
		this.mname = mname;
	}
	public int getLastreplyid() {
		return lastreplyid;
	}
	public void setLastreplyid(int lastreplyid) {
		this.lastreplyid = lastreplyid;
	}
	public String getLastreplyauthor() {
		return lastreplyauthor;
	}
	public void setLastreplyauthor(String lastreplyauthor) {
		this.lastreplyauthor = lastreplyauthor;
	}
	public Date getLastreplytime() {
		return lastreplytime;
	}
	public void setLastreplytime(Date lastreplytime) {
		this.lastreplytime = lastreplytime;
	}
	
	
	

}
