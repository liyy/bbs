package com.bbs.dto;

/**
 * 用户登录实体类
 *
 */
public class MemberLogin {

	/** 登录的用户名 */
	 private String loginname;
	 /** 密码  */ 
	 private String loginpwd;
	 /**验证码*/
	 private String logincode;
	 
	public String getLoginname() {
		return loginname;
	}
	public void setLoginname(String loginname) {
		this.loginname = loginname;
	}
	public String getLoginpwd() {
		return loginpwd;
	}
	public void setLoginpwd(String loginpwd) {
		this.loginpwd = loginpwd;
	}
	public String getLogincode() {
		return logincode;
	}
	public void setLogincode(String logincode) {
		this.logincode = logincode;
	}
	 
	 
}
