package com.bbs.dto;

import java.util.Date;



/**
 * @ClassName: LastTopic
 * 最后发表的帖子
 */
public class LastTopic {

	private int categoryid; //帖子所属版块id
	private int topicid;  //帖子id
	private String topictitle; //帖子的标题
	private String othername;  //作者name
	private Date tdateline;  //发表时间
	
	
	public int getTopicid() {
		return topicid;
	}
	public void setTopicid(int topicid) {
		this.topicid = topicid;
	}
	

	public String getTopictitle() {
		return topictitle;
	}
	public void setTopictitle(String topictitle) {
		this.topictitle = topictitle;
	}
	public String getOthername() {
		return othername;
	}
	public void setOthername(String othername) {
		this.othername = othername;
	}
	public Date getTdateline() {
		return tdateline;
	}
	public void setTdateline(Date tdateline) {
		this.tdateline = tdateline;
	}
	public int getCategoryid() {
		return categoryid;
	}
	public void setCategoryid(int categoryid) {
		this.categoryid = categoryid;
	}
	
	
}
