package com.bbs.dto;

import com.bbs.model.Member;
import com.bbs.model.Reply;

/**
 * @ClassName: TopicReply
 * 帖子表显示评论的封装
 * @accept:是否采纳（默认true）
 */
public class TopicReply {
	
	private Member member;
	private Reply reply;
	private boolean accept;  //是否采纳
	private int rscore;      //该评论所得分数（大于0）
	public TopicReply(){}
	public TopicReply(Member m, Reply r)
	{
		this.member = m;
		this.reply = r;
	}
	public Member getMember() {
		return member;
	}
	public void setMember(Member member) {
		this.member = member;
	}
	public Reply getReply() {
		return reply;
	}
	public void setReply(Reply reply) {
		this.reply = reply;
	}
	public boolean isAccept() {
		return accept;
	}
	public void setAccept(boolean accept) {
		this.accept = accept;
	}
	public int getRscore() {
		return rscore;
	}
	public void setRscore(int rscore) {
		this.rscore = rscore;
	}

	
}
