package com.bbs.dto;

import java.util.Date;
/**
 * 用户注册实体类
 */
public class MemberRegister {
	private int mid;
	private int rolesid;   //用户角色id
	private String mname;
	private String mpassword;
	private String mpassword2;
	private String verifykey;  //验证码
	private String mnickname;   //用户昵称 
	private String mheadpicurl; //头像地址
	private Date mregdate;      //注册时间
	private String msex;
	private Date mbitthday;
	private String maddress;
	private String mphone;
	private String memail;
	private String mqq;
	private int mintegral;  //用户积分
	private int mpostnum;   //发的总贴数
	private Date mlasttime;  //最后登录时间
	private String mlastip;  //最后登录IP地址
	private String msignature;  //个人签名
	private String mjob;     //干什么工作
	private String mnotes;  //备注
	private String power;  //用户权限
	

	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public int getRolesid() {
		return rolesid;
	}
	public void setRolesid(int rolesid) {
		this.rolesid = rolesid;
	}
	public String getMname() {
		return mname;
	}
	public void setMname(String mname) {
		this.mname = mname;
	}
	public String getMpassword() {
		return mpassword;
	}
	public void setMpassword(String mpassword) {
		this.mpassword = mpassword;
	}
	public String getMpassword2() {
		return mpassword2;
	}
	public void setMpassword2(String mpassword2) {
		this.mpassword2 = mpassword2;
	}
	public String getVerifykey() {
		return verifykey;
	}
	public void setVerifykey(String verifykey) {
		this.verifykey = verifykey;
	}
	public String getMnickname() {
		return mnickname;
	}
	public void setMnickname(String mnickname) {
		this.mnickname = mnickname;
	}
	public String getMheadpicurl() {
		return mheadpicurl;
	}
	public void setMheadpicurl(String mheadpicurl) {
		this.mheadpicurl = mheadpicurl;
	}
	public Date getMregdate() {
		return mregdate;
	}
	public void setMregdate(Date mregdate) {
		this.mregdate = mregdate;
	}
	public String getMsex() {
		return msex;
	}
	public void setMsex(String msex) {
		this.msex = msex;
	}
	public Date getMbitthday() {
		return mbitthday;
	}
	public void setMbitthday(Date mbitthday) {
		this.mbitthday = mbitthday;
	}
	public String getMaddress() {
		return maddress;
	}
	public void setMaddress(String maddress) {
		this.maddress = maddress;
	}
	public String getMphone() {
		return mphone;
	}
	public void setMphone(String mphone) {
		this.mphone = mphone;
	}
	public String getMemail() {
		return memail;
	}
	public void setMemail(String memail) {
		this.memail = memail;
	}
	public String getMqq() {
		return mqq;
	}
	public void setMqq(String mqq) {
		this.mqq = mqq;
	}
	public int getMintegral() {
		return mintegral;
	}
	public void setMintegral(int mintegral) {
		this.mintegral = mintegral;
	}
	public int getMpostnum() {
		return mpostnum;
	}
	public void setMpostnum(int mpostnum) {
		this.mpostnum = mpostnum;
	}
	public Date getMlasttime() {
		return mlasttime;
	}
	public void setMlasttime(Date mlasttime) {
		this.mlasttime = mlasttime;
	}
	public String getMlastip() {
		return mlastip;
	}
	public void setMlastip(String mlastip) {
		this.mlastip = mlastip;
	}
	public String getMsignature() {
		return msignature;
	}
	public void setMsignature(String msignature) {
		this.msignature = msignature;
	}
	public String getMjob() {
		return mjob;
	}
	public void setMjob(String mjob) {
		this.mjob = mjob;
	}
	public String getMnotes() {
		return mnotes;
	}
	public void setMnotes(String mnotes) {
		this.mnotes = mnotes;
	}
	
	public String getPower() {
		return power;
	}
	public void setPower(String power) {
		this.power = power;
	}
}
