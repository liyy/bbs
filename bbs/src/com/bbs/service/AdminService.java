package com.bbs.service;

import java.util.List;

import com.bbs.dto.MemberLogin;
import com.bbs.dto.MemberRegister;
import com.bbs.dto.SearchBean;
import com.bbs.model.Member;
import com.bbs.model.Topic;
import com.bbs.util.Page;

public interface AdminService {
	
	/**
	 * 后台管理员登录
	 * @return boolean 
     */
	boolean loginMember(MemberLogin memberlogin); 
	
	/**
	 * 添加管理员
	 * @return boolean    
	 */
	boolean addMember(MemberRegister register);
	
	/**
	 * 根据条件 读取会员集合
	 * @return List<Member>
	 */
	List<Member> loadMembers(SearchBean searchbean, Page page);
	
	/**
	 * 根据条件 读取帖子集合
	 * @return List<Topic>    
	 */
	List<Topic> loadTopics(SearchBean searchbean, Page page);
	
}
