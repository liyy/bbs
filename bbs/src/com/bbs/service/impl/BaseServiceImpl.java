package com.bbs.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.bbs.dao.BaseDao;
import com.bbs.dto.CategoryTopic;
import com.bbs.dto.LastTopic;
import com.bbs.dto.TopicReply;
import com.bbs.model.Announcements;
import com.bbs.model.Category;
import com.bbs.model.Links;
import com.bbs.model.Member;
import com.bbs.model.Navbar;
import com.bbs.model.Templates;
import com.bbs.model.Topic;
import com.bbs.service.BaseService;
import com.bbs.util.Page;

/**
 * @ClassName: BaseServiceImpl
 * 基础服务层实现类
 *
 */
@Component("baseservice")
public class BaseServiceImpl implements BaseService {

	private BaseDao basedao;

	public BaseDao getBasedao() {
		return basedao;
	}

	@Resource
	public void setBasedao(BaseDao basedao) {
		this.basedao = basedao;
	}

	                        //~~~~~~~~~~~~~index~~~~~~~~~~~~~~~~~~~//
	
	public List<Links> loadLinks() {
		
		return basedao.loadLinks();
	}

	public List<Navbar> loadNavbar() {

		return basedao.loadNavbar();
	}

	
	

	public List<Category> loadCategory() {

		return basedao.loadCategory();
	}

	public List<LastTopic> lastTopic() {
		
		return basedao.lastTopic();
	}
	                     //~~~~~~~~~~Category~~~~~~~~~~~~~~~//
	public Category getCategory(int Categoryid) {

		return basedao.getCategory(Categoryid);
	}

	public List<Announcements> loadAnnouncements(int categoryid) {

		return basedao.loadAnnouncements(categoryid);
	}

	public List<CategoryTopic> loadCategoryTopic(int categoryid, Page page) {

		return basedao.loadCategoryTopic(categoryid, page);
	}
	                        //~~~~~~~~~~Topic~~~~~~~~~~~~~~~//
	public Topic loadTopic(int topicid) {

		return basedao.loadTopic(topicid);
	}

	public List<TopicReply> loadTopicReply(int topicid) {

		return basedao.loadTopicReply(topicid);
	}

	public Member publihMember(int topicid) {

		return basedao.publihMember(topicid);
	}

	

	

	
	
}
