package com.bbs.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.bbs.dao.MemberDao;
import com.bbs.dto.MemberLogin;
import com.bbs.dto.MemberRegister;
import com.bbs.model.Member;
import com.bbs.model.Topic;
import com.bbs.service.MemberService;

@Component("memberservice")
public class MemberServiceImpl implements MemberService {

	private MemberDao memberdao;
	
	public MemberDao getMemberdao() {
		return memberdao;
	}

	@Resource
	public void setMemberdao(MemberDao memberdao) {
		this.memberdao = memberdao;
	}

	public Member exists(String name) {
		// TODO Auto-generated method stub
		return memberdao.exists(name);
	}

	public boolean loginMember(MemberLogin memberlogin) {
		
		return memberdao.loginMember(memberlogin);
	}

	public boolean registerMember(MemberRegister member) {
		return memberdao.registerMember(member);
	}

	public boolean addtopic(Topic topic) {
		// TODO Auto-generated method stub
		return false;
	}

	

}
