package com.bbs.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.bbs.dao.AdminDao;
import com.bbs.dao.BaseDao;
import com.bbs.dao.MemberDao;
import com.bbs.dto.MemberLogin;
import com.bbs.dto.MemberRegister;
import com.bbs.dto.SearchBean;
import com.bbs.model.Member;
import com.bbs.model.Topic;
import com.bbs.service.AdminService;
import com.bbs.util.Page;

@Component("adminservice")
public class AdminServiceImpl implements AdminService {

	private AdminDao admindao;
	private MemberDao memberdao;
	
	public AdminDao getAdmindao() {
		return admindao;
	}

	@Resource
	public void setAdmindao(AdminDao admindao) {
		this.admindao = admindao;
	}
	
	public MemberDao getMemberdao() {
		return memberdao;
	}

	@Resource
	public void setMemberdao(MemberDao memberdao) {
		this.memberdao = memberdao;
	}
	                                     //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~/
	public boolean addMember(MemberRegister register) {
		if(memberdao.exists(register.getMname())!=null )
		{
			return false;
		}
		if(memberdao.registerMember(register))
		{
			return true;
		}
		return false;
	}

	public boolean loginMember(MemberLogin memberlogin) {

		if(admindao.loginMember(memberlogin))
		{
			return true;
		}
		return false;
	}

	public List<Member> loadMembers(SearchBean searchbean, Page page) {
		
		return admindao.loadMembers(searchbean, page);
	}

	public List<Topic> loadTopics(SearchBean searchbean, Page page) {
		return admindao.loadTopics(searchbean, page);
	}

	

	
}
