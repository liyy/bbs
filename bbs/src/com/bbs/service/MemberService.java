/**  
* @Title: MemberService.java
* @Package com.bbs.service
* @Description: TODO(会员服务层)
* @author 
* @date 2010-11-19 下午01:37:40
* @version V1.0  
*/
package com.bbs.service;

import com.bbs.dto.MemberLogin;
import com.bbs.dto.MemberRegister;
import com.bbs.model.Member;
import com.bbs.model.Topic;


public interface MemberService {
	
	/**
	 * 根据名称判断 是否有该用户
	 * @return Member
     */
	Member exists(String name);
	
	/**
	 * 用户登录
	 * @return boolean 
     */
	boolean loginMember(MemberLogin memberlogin); 
	
	/**
	 * 用户注册
   	 * @return boolean    
   	 */
	boolean registerMember(MemberRegister member);
	
	/**
	 * 会员发帖
	 * @return boolean    返回类型
	 */
	boolean addtopic(Topic topic);
}
