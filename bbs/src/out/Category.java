package out;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.bbs.model.Topic;

/**
 * 模块实体
 *
 */
@Entity
public class Category {

	private int categoryid;
	
	/**属于版块*/
	private Category fcategoryid; 
	
	//private int templateid; //所属版块的id
	//private Templates templateid;
	private String ctitle;  //模板名称
	private String cicon;   //模板图标
	private String cdescription; //模板简介
	private boolean cstatus;  //是否在论坛上显示
	//private int  clasttopicid; //最后发表的帖子
	//private Topic clasttopicid; 
	
	/**该版下面有哪些版块 */
	private Set<Category> categorys = new HashSet<Category>();  
	
	@Id
	@GeneratedValue
	public int getCategoryid() {
		return categoryid;
	}
	

	/**
	 *  属于那个版块    多对一 
   	 * @return Category   
	 */
	@ManyToOne
	@JoinColumn(name="fcategoryid")
    public Category getFcategoryid() {
		return fcategoryid;
	}
	
	/**
	 * 该版块下最后发表的帖子    一对一
	* @return Topic    
	 */
	@OneToOne
	@JoinColumn(name="clasttopicid")
//	public Topic getClasttopicid() {
//		return clasttopicid;
//	}

	/**
	 *  该版下面有所有子版块
	 * @return Set<Category>   
	 */
//	@OneToMany
//	@JoinColumn(name="category")
//	public Set<Category> getCategorys() {
//		return categorys;
//	}


	public void setCategorys(Set<Category> categorys) {
		this.categorys = categorys;
	}

//
//	public void setClasttopicid(Topic clasttopicid) {
//		this.clasttopicid = clasttopicid;
//	}


	public void setCategoryid(int categoryid) {
		this.categoryid = categoryid;
	}
	public void setFcategoryid(Category fcategoryid) {
		this.fcategoryid = fcategoryid;
	}
	
	//	/**
//	* 小模块外键 templateid所属版块的id  
//	* 多对一
//	 */
//	@ManyToOne
//	@JoinColumn(name="templateid")
//	public Templates getTemplateid() {
//		return templateid;
//	}
//	public void setTemplateid(Templates templateid) {
//		this.templateid = templateid;
//	}
	public String getCtitle() {
		return ctitle;
	}
	public void setCtitle(String ctitle) {
		this.ctitle = ctitle;
	}
	public String getCicon() {
		return cicon;
	}
	public void setCicon(String cicon) {
		this.cicon = cicon;
	}
	public String getCdescription() {
		return cdescription;
	}
	public void setCdescription(String cdescription) {
		this.cdescription = cdescription;
	}
	public boolean isCstatus() {
		return cstatus;
	}
	public void setCstatus(boolean cstatus) {
		this.cstatus = cstatus;
	}
	
}
