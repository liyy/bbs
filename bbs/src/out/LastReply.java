package out;

import java.util.Date;

import com.bbs.model.Reply;

/**
 * @ClassName: LastCategory
 * 最后回复的评论 
 */
//废弃
public class LastReply {

	private int replyid;  //评论id
	private String othername;   //发表的作者
	private Date createTime;   //发表时间
	
	
	public int getReplyid() {
		return replyid;
	}
	public void setReplyid(int replyid) {
		this.replyid = replyid;
	}
	public String getOthername() {
		return othername;
	}
	public void setOthername(String othername) {
		this.othername = othername;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	
}
