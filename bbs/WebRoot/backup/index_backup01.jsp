<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	<title>首页</title>

	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">

	<link rel="stylesheet" type="text/css" href="css/base.css">
  </head>
  
 <body>
<div id="page">
  <div id="top">
    <div id="topbar"> 
    <span class="topbar_left"> 
    <s:if test="#session.membername!=null">
       <span class="span_username"> <a class="a_topmenu"  href="#"> <img class="top_face .left" src="images/9.jpg"/> ${sessionScope.membername} </a></span>
       <a href="#" >退出</a> <i class="left">&nbsp;</i> <a href="#" >设置</a> 
       <a href="#" >任务(2)</a> <a class="new_topbar_mes" href="#" >消息 <span class="green" >(3)</span> </a>
    </s:if>
    <s:else>
    <a href="#" >退出</a> <i class="left">&nbsp;</i> <a href="#" >注册</a> 
    </s:else>
   </span>
      <ul class="topbar_right right">
        <li> <a href="#">www</a> </li>
        <li> <a href="#">www</a> </li>
        <li> <a href="#">www</a> </li>
        <li> <a href="#">帮助</a> </li>
      </ul>
    </div>
  </div>
  <div id="header">
    <div id="topmiddle" >
      <div class="topmiddle_left left"> <a class="aa" href="#"> <img src="images/logo.png"/> </a> </div>
      <div class="topmiddle_right right"> <a href="#"> <img  src="images/php100guangpan.jpg" /> </a> </div>
    </div>
    <div id="topdown_nav">
      <div id="topnav_left">
        <ul>
        
        <s:iterator value="navbar" status="nav" var="onav" >
        
          <li <s:if test="#nav.index==0" >class="nav_current"</s:if>>
          <a href="<s:property value="onav.getNavurl()" />"><s:property/> </a>
          </li>
         
        </s:iterator>
        </ul>
      </div>
      <div id="topnav_right"> <a class="right s_g" href="#">高级</a>
        <form method="post" action="#">
          <input id="" class="s_text" type="text" />
          <input id="" class="s_btn" type="button" />
        </form>
      </div>
    </div>
  </div>
  <!--end header-->
  
  <div class="index_info">
    <div class="index_info_left left"> <span> <a href="#">我的主题</a> <a href="#">我的回复</a> <a href="#">我的收藏</a> <a href="#">好友近况</a> </span> </div>
    <div class="index_info_right right"> <span class="right"> <a href="#">最新帖子</a> <a href="#">精华区</a> <a href="#">会员列表</a> <a href="#">统计排行</a> </span> </div>
  </div>
  <!--end index_info-->
  <div class="clearThis"></div>
  <div id="main">
    <div class="forum_info cc gray">
      <div class="right" > 主题:45581 | 帖子:517475 | 会员:87122 | 新会员: <span> <a href="#"></a> </span> <a title="RSS订阅本版面最新帖子" target="_blank" href="#"> <img src="images/rss.png"/> </a> </div>
      <div class="left"> 今日:1234 | 昨日:1366 |  	最高日:2836 | </div>
    </div>
    <!--end forum_info-->
    
    <div id="location" class="cc"> <span class="right" >版主：&nbsp;<a href="#">张三</a> </span> <img align="absmiddle"  src="images/home.gif"/> <a title="PHP100中文网论坛" href="#">PHP100中文网论坛</a> &raquo; <a href="#">XHTML / CSS / Script / Ajax</a> <a title="RSS订阅本版面最新帖子" target="_blank" href="#"> <img align="absbottom" style=" margin-top:2px" src="images/rss.png" /> </a> </div>
    <!--end location-->
    <div id="content">
  
      <div class="d">
        <div class="h"> <a class="right"> <img src="images/cate_fold.gif" /> </a> &raquo;
          <h2> <a href="#">PHP100专栏</a> </h2>
        </div>
      </div>
      <s:iterator value="templates" var="tl" >
      <div class="d">
        <div class="h"> <a class="right"> <img src="images/cate_fold.gif" /> </a> &raquo;
          <h2> <a href="#"><s:property value="#tl.getTempname()"/> <!-- 获取大板块标题 --></a> </h2>
        </div>
        <div class="m_content">
          <ul class="tr2" >
            <li class="lic trcli" >版块</li>
            <li class="fleft  e" >主题 / 文章</li>
            <li class="fleft  f" >最后发表</li>
          </ul>
          <div class="clearThis"></div>
          
        <s:iterator value="categorys" var="category" ><!-- 各个小板块 -->
        <s:if test="#category.getTemplateid()==#tl.getTemplateid()">
          <ul class="ul_content">
            <li class="lic li1"> <a href="#"> <img src="images/new.gif"/> </a> </li>
            <li class="li2">
              <div class="b left li2hh li2_div1"> <a href="#"><s:property value="#category.getCtitle()"/><!-- 板块标题 -->  </a> </div>
              <div class="s3 li2hh">&nbsp;(今天: 43)</div>
              <div class="li_div1"><s:property value="#category.getCdescription()" /><!-- 版块简介 -->  </div>
              <div class="gray li2hh">版主:libailin&nbsp; xiaohufei&nbsp; 莫愁 </div>
            </li>
            <li class="li3 e">
              <div class="s3 left">13447</div>
              <div class="left">/</div>
              <div class="f10 left">92658</div>
            </li>
            <s:iterator value="topics" var="topic" >
            <s:if test="#topic.getCategoryid()==#category.getTemplateid()" >
	            <li class="li4"> <a class="li2hh" href="#"><s:property value="#topic.getTopictitle()" /><!--最后发表帖子的标题  --> </a><br/>
	              <div class="li2hh gray"><s:property value="" /><!-- 帖子作者 --> </div>
	              <div class="li2hh gray f9"><s:property value="topic.getTdateline()" /><!-- 帖子发表时间 --> </div>
	            </li>
            </s:if>
            </s:iterator>
          </ul>
          </s:if>
        </s:iterator>
        
          <!--end ul-->
          <div class="clearThis"></div>
          <ul class="ul_content">
            <li class="lic li1"> <a href="#"> <img src="images/new.gif"/> </a> </li>
            <li class="li2">
              <div class="b left li2hh li2_div1"> <a href="#">新手问答求助区 - 悬赏区</a> </div>
              <div class="s3 li2hh">&nbsp;(今天: 43)</div>
              <div class="li_div1">新手乐园，当你遇到问题时到这里来求助一下吧，热情的会员和斑竹会给你满意的答案。回答问题还能获得加分 </div>
              <div class="gray li2hh">版主:libailin&nbsp; xiaohufei&nbsp; 莫愁 </div>
            </li>
            <li class="li3 e">
              <div class="s3 left">13447</div>
              <div class="left">/</div>
              <div class="f10 left">92658</div>
            </li>
            <li class="li4"> <a class="li2hh" href="#">PHP中出现JS乱码</a><br/>
              <div class="li2hh gray">丫头2010 在线 </div>
              <div class="li2hh gray f9">2010-11-04 12:51</div>
            </li>
          </ul>
          <!--end ul--> 
        </div>
      </div>
      <!--end d-->
  </s:iterator>
      <div id="d_content">
          <div class="d" >
            <div class="h"> <a class="right"> <img src="images/cate_fold.gif" /> </a> 
              <h2>站点相关</h2>
            </div>
            <div class="d_h">
            &raquo;友情链接
            </div>
            
            <ul class="ul_content cd_ul">
            <li class="lic li1"> <a href="#"> <img src="images/new.gif"/> </a> </li>
            <li class="d_web d_hight">
              <s:iterator value="links" var="link" >
               <s:if test="#link.getLinkimg()!=null">
                <a target="_blank" href="<s:property value="#link.getLinkurl()"/>">
               	  <img class="w_img" alt="<s:property value="#link.getLinktitle()"/>"  src="<s:property value="#link.getLinkimg()"/>" />
                </a>
                </s:if>
                <s:else>
                   <a title="" target="_blank" href="#"><s:property value="#link.getLinktitle()"/></a>
                </s:else>
              </s:iterator>
              
                <a target="_blank" href="#">
                <img class="w_img" alt="PHP100中文站" src="images/logo.gif"/>
                </a>
               
             <br/>
               <a title="" target="_blank" href="#">[联盟主机网]</a>
            </li>
            </ul>
          </div>
        </div>
        <!--end d_content--> 
    </div>
  </div>
  <!--end main-->
  
  <div id="footer"> 
  	<div >
    	<div class=" bottom cc ">
        <ul class="lic">
            <li><a href="#">清除Cookies</a></li>
            <li><a target="_blank" href="#">关于我们</a></li>
            <li><a target="_blank" href="#">管理团队</a></li>
            <li><a target="_blank" href="#">广告服务</a></li>
            <li><a href="#">无图版</a></li>
            <li><a href="#">手机浏览</a></li>
            <li><a href="#">提建议</a></li>
            <li><a href="javascript:scroll(0,0)">返回顶部</a></li>
		</ul>
        </div>
       <center  class="f9 gray" >
       <span>Total 0.008953(s) query 3, Time now is:11-04 21:41, Gzip disabled </span><br/>
       Powered by 
       <a href="#"><b>ANDRORD</b></a>
       </center>
    </div>
  </div>
  <!--end footer--> 
  
</div>
</body>
</html>
