<%@ page language="java" import="java.util.*" pageEncoding="GBK"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><s:property value="category.getCtitle()" /> </title><!--子版块  -->

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
	
		<link rel="stylesheet" type="text/css" href="../css/base.css">
		<script type="text/javascript" src="../js/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../js/jquery.paginate.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/page.css">
	</head>

	<body>
		<div id="page">

			<%@include file="../base/_header.jsp" %>
           <!-- top, header, index_info  -->
			<div class="clearThis"></div>
			<div id="main">
				<div class="forum_info cc gray">
					<div class="right">
						主题:45581 | 帖子:517475 | 会员:87122 | 新会员:
						<span> <a href="#"> </a> </span>
						<a title="RSS订阅本版面最新帖子" target="_blank" href="#"> <img
								src="../images/rss.png" /> </a>
					</div>
					<div class="left">
						今日:1234 | 昨日:1366 | 最高日:2836 |
					</div>
				</div>
				<!--end forum_info-->

				<div id="location" class="cc">
					<span class="right">版主：&nbsp; <a href="#"> 张三 </a> </span>
					<img align="absmiddle" src="../images/home.gif" />
					<a title="PHP100中文网论坛" href="#"> PHP100中文网论坛 </a> >>>
					<a href="#"> XHTML / CSS / Script / Ajax </a>
					<a title="RSS订阅本版面最新帖子" target="_blank" href="#"> <img
							align="absbottom" style="margin-top: 2px" src="../images/rss.png" />
					</a>
				</div>
				<!--end location-->

				<div id="content">
				<s:iterator value="catannoun" status="cata" var="announ" >
				  <s:if test="#cata.index==0&&announ.size()>0&&#announ!=null">
					<div class="d">
						<div class="h">
							<div class="hg pr cc">
								<a class="right"> <img src="../images/cate_fold.gif" /> </a>
								<div class="gg threadlist">
									<a class="left active" target="_blank" href="#">版块公告</a>
								</div>
							</div>
						</div>
						<div class="dh">
							版块简介: <s:property value="category.getCdescription()"/>
						</div>

						<div class="gdiv p10">
							<span class="gfont"> <font>
								<s:property value="#announ.getMessage()"/><!-- 公共内容 -->
							</font> </span>
							<div class="clearThis"></div>
							<div class="right">
								<a class="gray" target="_blank" href="#">更多</a>
							</div>
						</div>
					</div>
					</s:if>
					</s:iterator>
					<!--end d-->
					
					<%@include file="../base/_page.jsp" %>
					<!--分页-->
					<div class="clearThis"></div>
					<div class="d">
						<div class="h">
							<div class="tzh pr cc">
								<div class="left threadlist ">
									<span> <a class="current" href="#">全部</a>
									</span>
									<span class="stzh"> 
									<a class="dil" href="#">精华</a> 
									<s:iterator value="topictypes" var="ttype" >
									<s:if test="#ttype.getCategoryid()==category.getCategoryid()">
										<a class="dil" href="#"><b><s:property value="#ttype.getTypecontent()" /> </b></a>
									</s:if>
									</s:iterator>
									</span>
								</div>

							</div>
						</div>

						<table class="tzb">
							<tr class="tr2 trh">
								<td class="lic tdh1"></td>
								<td class="lic tdh2">
									文章
								</td>
								<td class="tdh3">
									作者
								</td>
								<td class="tdh4">
									回复/人气
								</td>
								<td class="tdh5">
									最后发表
								</td>
							</tr>
							<!--end tr-->
							<s:iterator value="catetopics" var="cat"  >
							<tr class="tr3 t_one t_h">
								<td class="lic">
									<img src="../images/anc.gif" />
								</td>
								<th>
									&nbsp;<s:property value="#cat.getTypecontent()" />
									<a href="#">
									<font color="red"><s:property value="#cat.getTopictitle()"/> <!-- 帖子标题 --> </font>
									</a>
								</th>
								<td class="fleft">
									<a href="#"><s:property value="#cat.getMname()"/>作者</a>
								</td>
								<td class="fleft tr_rep">
									<span>9</span>/1123
								</td>
								<td class="fleft">
									<span class="o_h"><a href="#"><s:property value="#cat.getLastreplyauthor()"/><!--最后回复评论的作者  --> </a>
									</span>
									<br />
									<span class="f10 gray left o_h"><s:property value="#cat.getLastreplytime()"/> </span>
								</td>
							</tr>
						   </s:iterator>
						   
							<!--end tr-->

							<tr class="tr3 t_one  t_h">
								<td class="lic">
									<img src="../images/anc.gif" />
								</td>
								<th>
									&nbsp;站点公告:
									<a href="#"><font color="red">PHP100邀请您的加盟 [上海]
											创恩信息技术有限公司</font>
									</a>
								</th>
								<td class="fleft">
									<a href="#">作者</a>
								</td>
								<td class="fleft tr_rep">
									<span>9</span>/1123
								</td>
								<td class="fleft">
									<span class="o_h"><a href="#">王二</a>
									</span>
									<br />
									<span class="f10 gray left o_h">2010-11-5 14:28</span>
								</td>
							</tr>
							<!--end tr-->

						</table>
					</div>
				</div>
				<!--end content-->
			</div>
			<%@include file="../base/_page.jsp" %>
		   <!--分页-->
		
		<!--end main-->

		<div id="footer">
			<div>
				<div class=" bottom cc ">
					<ul class="lic">
						<li>
							<a href="#">清除Cookies</a>
						</li>
						<li>
							<a target="_blank" href="#">关于我们</a>
						</li>
						<li>
							<a target="_blank" href="#">管理团队</a>
						</li>
						<li>
							<a target="_blank" href="#">广告服务</a>
						</li>
						<li>
							<a href="#">无图版</a>
						</li>
						<li>
							<a href="#">手机浏览</a>
						</li>
						<li>
							<a href="#">提建议</a>
						</li>
						<li>
							<a href="javascript:scroll(0,0)">返回顶部</a>
						</li>
					</ul>
				</div>
				<center class="f9 gray">
					<span>Total 0.008953(s) query 3, Time now is:11-04 21:41,
						Gzip disabled </span>
					<br />
					Powered by
					<a href="#"><b>ANDRORD</b>
					</a>
				</center>
			</div>
		</div>
		<!--end footer-->

		</div>
	</body>
</html>
