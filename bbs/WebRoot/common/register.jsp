<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	<title>用户注册</title>

	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<script src="../js/jquery-1.4.3.min.js" type="text/javascript"></script>
	<script src="../js/jquery.validate.min.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="../css/base.css">
<script type="text/javascript">
$().ready(function() {
	// validate the comment form when it is submitted
	$("#commentForm").validate();
	
	// validate signup form on keyup and submit
	$("#signupForm").validate({
		rules: {
		    mname: {
				required: true,
				minlength: 3
			},
			mpassword: {
				required: true,
				minlength: 5
			},
			mpassword2: {
				required: true,
				minlength: 5,
				equalTo: "#password"
			},
			memail: {
				required: true,
				email: true
			},
			topic: {
				required: "#newsletter:checked",
				minlength: 2
			},
			verifykey:"required",
			agree: "required"
		},
		messages: {
			mname: {
				required: "请输入用户名",
				minlength: "用户名至少3个字符"
			},
			mpassword: {
				required: "请输入密码",
				minlength: "密码至少5个字符"
			},
			mpassword2: {
				required: "请输入密码确认",
				minlength: "密码确认至少5个字符",
				equalTo: "两次输入的密码不相等"
			},
			memail: "请输入正确的email地址",
			verifykey:"验证码不能为空",
			agree: "未选择条款内容"
		}
	});
	
});
</script>
  </head>
  
  <body>
<div id="page">
  <div id="top">
    <div id="topbar"> <span class="topbar_left"> <span class="span_username"> <a class="a_topmenu"  href="#"> <img class="top_face .left" src="../images/9.jpg"/> javac </a> </span> <a href="#" >退出</a> <i class="left">&nbsp;</i> <a href="#" >设置</a> <a href="#" >任务(2)</a> <a class="new_topbar_mes" href="#" >消息 <span class="green" >(3)</span> </a> </span>
      <ul class="topbar_right right">
        <li> <a href="#">www</a> </li>
        <li> <a href="#">www</a> </li>
        <li> <a href="#">www</a> </li>
        <li> <a href="#">帮助</a> </li>
      </ul>
    </div>
  </div>
  <div id="header">
    <div id="topmiddle" >
      <div class="topmiddle_left left"> <a class="aa" href="#"> <img src="../images/logo.png"/> </a> </div>
      <div class="topmiddle_right right"> <a href="#"> <img  src="../images/php100guangpan.jpg" /> </a> </div>
    </div>
    <div id="topdown_nav">
      <div id="topnav_left">
        <ul>
          <li class="nav_current"><a href="#">论坛首页</a></li>
          <li><a href="#">圈子</a></li>
          <li><a href="#">圈子</a></li>
          <li><a href="#">圈子</a></li>
          <li><a href="#">网站地图</a></li>
        </ul>
      </div>
      <div id="topnav_right"> <a class="right s_g" href="#">高级</a>
        <form method="post" action="#">
          <input id="" class="s_text" type="text" />
          <input id="" class="s_btn" type="button" />
        </form>
      </div>
    </div>
  </div>
  <!--end header-->
  
  <div class="index_info">
    <div class="index_info_left left"> <span> <a href="#">我的主题</a> <a href="#">我的回复</a> <a href="#">我的收藏</a> <a href="#">好友近况</a> </span> </div>
    <div class="index_info_right right"> <span class="right"> <a href="#">最新帖子</a> <a href="#">精华区</a> <a href="#">会员列表</a> <a href="#">统计排行</a> </span> </div>
  </div>
  <!--end index_info-->
  <div class="clearThis"></div>
  <div id="main">

    
    <div id="location" class="cc"><img align="absmiddle"  src="../images/home.gif"/> <a title="PHP100中文网论坛" href="#">PHP100中文网论坛</a> &raquo;<span class="tr_rep">注册</span></div>
    <!--end location-->
    <div id="register">
     <div id="breadCrumb" class="cc b f16">注册</div>
      <div>
      	<div class="y-bg"></div>
        <div class="y-bg2"></div>
        <div class="y-bg3"></div>
        <div class="y-bg4">
        	<div class="p10 cc">
            <div class="cc b p10 f14 ">请填写以下必填信息完成注册</div>
            
            <form id="signupForm" class="cmxform" method="post" action="<%=request.getContextPath() %>/member/member_register">
	            <div class="cc p10 regItem">
	            <fieldset >
	            <p>
	            <label for="Firstname">用户名</label>
	            <input id="Firstname" type="text"  name="mname" />
	            </p>
	            <p>
	             <label for="password">密  码</label>
	            <input id="password" type="password"  name="mpassword" />
	            </p>
	            <p>
	             <label for="confirm_password">确认密码</label>
	            <input id="confirm_password" type="password"  name="mpassword2" />
	            </p>
	             <p>
	             <label for="email">Email</label>
	            <input id="email" type="text"  name="memail" />
	            </p>
	            <p>
	            <label for="num">认证码</label>
	            <input id="num" type="text"  name="verifykey" />
	            <img src="" align="absmiddle" alt="看不清楚，换一张" onclick="" />
	            <span class="hand">看不清楚，换一张</span>
	            </p>
	            <p>
	            <label for="agree">&nbsp;我已阅读并完全同意</label>
	            <input id="agree" class="checkbox" type="checkbox"  name="agree" />
	            &nbsp;我已阅读并完全同意
	            <a class="b hand"  onclick="">条款内容</a>
	            </p>
	            <p>
	            <input class="submit" type="submit" value="提交注册" />
	            <input class="reset" type="reset" value="重 置"/>
	            </p>
	            </fieldset>
	            </div>
            </form>
            </div>
        </div>
        <div class="y-bg3"></div>
        <div class="y-bg2"></div>
        <div class="y-bg"></div>
      </div>
      
      <!--end content-->
    </div>
  </div>
  <!--end main-->
  
  <div id="footer"> 
  	<div >
    	<div class=" bottom cc ">
        <ul class="lic">
            <li><a href="#">清除Cookies</a></li>
            <li><a target="_blank" href="#">关于我们</a></li>
            <li><a target="_blank" href="#">管理团队</a></li>
            <li><a target="_blank" href="#">广告服务</a></li>
            <li><a href="#">无图版</a></li>
            <li><a href="#">手机浏览</a></li>
            <li><a href="#">提建议</a></li>
            <li><a href="javascript:scroll(0,0)">返回顶部</a></li>
		</ul>
        </div>
       <center  class="f9 gray" >
       <span>Total 0.008953(s) query 3, Time now is:11-04 21:41, Gzip disabled </span><br/>
       Powered by 
       <a href="#"><b>ANDRORD</b></a>
       </center>
    </div>
  </div>
  <!--end footer--> 
  
</div>
</body>
</html>
