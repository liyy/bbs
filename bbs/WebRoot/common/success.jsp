<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	<title>无标题文档</title>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">

	<link rel="stylesheet" type="text/css" href="../css/middle.css"/>

  </head>

 <body>
<div id="tip_page">
	<div id="tip_bgA"></div>
    <div id="tip_bgB"></div>
	<div id="tip_content">
    	<div id="tip_top"></div>
        <div id="tip_dow">
            <span class="f14"><s:property value="memberResult.msg"/> </span> <!-- 提示信息 -->
            <br/>
            <div class="tip_dc">
            <a class="showback" href="<s:property value="memberResult.url"/>" onclick="var needReload = (is_ie ? history.length == 0 : history.length == 1); if(needReload){self.location.reload();}else{history.back();}return false;">返回继续操作</a>
            </div>
            <a class="showindex" href="index.jsp">返回首页</a>
        </div>
    </div>
     <div id="tip_bgB"></div>
     <s:debug></s:debug>
</div>
</body>
</html>
