<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.fckeditor.net" prefix="FCK" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	<title>帖子</title>

	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">

	<link rel="stylesheet" type="text/css" href="../css/base.css"/>

  </head>
  
  <body>
<div id="page">
   <input type="hidden" id="actionname" value="topic.action"/>
  <%@include file="../base/_header.jsp" %>
  <!-- top, header, index_info  -->
  <div class="clearThis"></div>
  <div id="main">

      <div id="location" class="cc">  <img align="absmiddle"  src="../images/home.gif"/> <a title="PHP100中文网论坛" href="#">PHP100中文网论坛</a>&raquo;<a href="#">XHTML / CSS / Script / Ajax</a> &raquo;<a href="#">XHTML / CSS / Script / Ajax</a> <a title="RSS订阅本版面最新帖子" target="_blank" href="#"> <img align="absbottom" style=" margin-top:2px" src="../images/rss.png" /> </a> </div>
    <!--end location-->
    <%@include file="../base/_page.jsp" %>
    <input type="hidden" id="ationname" value="topic.action" />
    <!-- 分页 -->
    <!--end -->
    <div class="t">
    	<div class="h">
        主题 : <s:property value="topic.getTopictitle()" />
        <s:if test="topicstate==true">
        <span class="check">[问题点数:<s:property value="topic.getTfraciton()"/> ，结帖人:<s:property value="member.getMname()"/>]</span>
        </s:if>
        <s:elseif test="topic.getTfraciton()!=0" >
        <span>分数：<s:property value="topic.getTfraciton()"/></span>
        </s:elseif>
        
        </div>
    </div>
    <div class="t5 toprim1">
            <div class="topicl left">
            <div class="lname cc "><b class="left black"><a href="#"><s:property value="member.getMname()"/></a></b></div><!-- 用户名 -->
            <div class="userpic"><a href="#"><img src="<s:property value="member.getMheadpicurl()"/>"/></a></div><!-- 个人图片 -->
            <div class="userrank">
            级别: 圣骑士<br/>
            <img src="../images/10.png"/>
            </div>
            
            <span class="ss1">
            <div class="clearThis"></div>
            发帖:
            <span class="s1"><s:property value="member.getMpostnum()"/> </span><br/>
            积分：
            <span class="s2"><s:property value="member.getMintegral()"/> </span><br/>
            注册时间: 
            <span class="gray"><s:property value="member.getMregdate()"/></span><br/>
            最后登录:
            <span class="gray"><s:property value="member.getMlasttime()"/></span><br/>
            </span>
            </div>
            <!--end topicl-->
            <div class="topicr left ">
            	<div class="tiptop cc">
                <div class="left"><a class="s2" href="#">楼主</a></div>
                <div class="left gray">发表于: <s:property value="topic.getTdateline()" /> </div>
                </div>
                <div class="tipmiddle">
               <s:property value="topic.getTcontent()" /><!--帖子内容  -->
                </div>
                </div>
                <!--end middle-->
            	<div class="tipdow">
                <div class="sigline"></div>
                <div class="signature"><s:property value="member.getMsignature()"/></div><!-- 个人签名 -->
                <div class="tipad cc">
                <div class="left readbot"><a href="#">回复</a><a href="#">引用</a></div>
                </div>
                </div>
            </div>
            <!--end topicr-->
    </div>
    <!--end t5-->
    <s:iterator value="topicreplys" status="trs"  var="listreply" >
     
     <div class="t5 <s:if test="#listreply.isAccept()">toprim3</s:if><s:else>toprim2</s:else> ">
            <div class="topicl left">
            <div class="lname cc "><b class="left black"><a href="#"><s:property value="#listreply.getMember().getMname()" /> </a></b></div>
            <div class="userpic"><a href="#"><img src="<s:property value="#listreply.getMember().getMheadpicurl()" />"/></a></div>
            <div class="userrank">
            级别: 圣骑士<br/>
            <img src="../images/10.png"/>
            </div>
            
            <span  class="ss1">
            <div class="clearThis"></div>
            发帖:
            <span class="s1"><s:property value="#listreply.getMember().getMpostnum()" /></span><br/>
            积分：
            <span class="s2"><s:property value="#listreply.getMember().getMintegral()" /></span><br/>
            注册时间: 
            <span class="gray"><s:property value="#listreply.getMember().getMregdate()" /></span><br/>
            最后登录:
            <span class="gray"><s:property value="#listreply.getMember().getMlasttime()" /></span><br/>
            </span>
            </div>
            <!--end topicl-->
            <div class="topicr left ">
            	<div class="tiptop cc">
                <div class="left"><a class="s2" href="#"><s:if test="#trs.count==1">沙发</s:if><s:else>#<s:property value="#trs.count" /></s:else>  </a></div>
                <div class="left gray">发表于:<s:property value="#listreply.getReply().getRcreatetime()" />  </div>
                <s:if test="#listreply.isAccept()">
	                <div class="right topimg">
	                <img src="../images/trans.gif" /> 得分：<s:property value="#listreply.getRscore()" />
	                </div> 
                </s:if>
                <s:else>
                  <div class="right">得分：0</div>
                </s:else> 
                </div>
                <div class="tipmiddle">
               <s:property value="#listreply.getRcontent()" /><!--评论内容  -->
                </div>
            	 <!--end middle-->
            	<div class="tipdow">
                <div class="sigline"></div>
                <div class="signature"><s:property value="#listreply.getMember().getMsignature()" /></div><!-- 个人签名 -->
                <div class="tipad cc">
                <div class="left readbot"><a href="#">回复</a><a href="#">引用</a></div>
                </div>
                </div>
            </div>
            <!--end topicr-->
    </div>
    <!--end t5-->
    </s:iterator>
     <div class="t5 toprim3">
            <div class="topicl left">
            <div class="lname cc "><b class="left black"><a href="#">******</a></b></div>
            <div class="userpic"><a href="#"><img src="../images/none.gif"/></a></div>
            <div class="userrank">
            级别: 圣骑士<br/>
            <img src="../images/10.png"/>
            </div>
            
            <span  class="ss1">
            <div class="clearThis"></div>
            发帖:
            <span class="s1">146</span><br/>
            积分：
            <span class="s2">13</span><br/>
            注册时间: 
            <span class="gray">2010-11-7</span><br/>
            最后登录:
            <span class="gray">2010-11-7</span><br/>
            </span>
            </div>
            <!--end topicl-->
            <div class="topicr left ">
            	<div class="tiptop cc">
                <div class="left"><a class="s2" href="#">二楼</a></div>
                <div class="left gray">发表于: 2009-05-03</div>
                <div class="right topimg"><img src="../images/trans.gif" /> 得分：0</div>
                </div>
                <div class="tipmiddle">
               ssssssssssssss
                </div>
            	 <!--end middle-->
            	<div class="tipdow">
                <div class="sigline"></div>
                <div class="signature">个人签名!!!!!!!!!!!!!!</div>
                <div class="tipad cc">
                <div class="left readbot"><a href="#">回复</a><a href="#">引用</a></div>
                </div>
                </div>
            </div>
            <!--end topicr-->
    </div>
    <!--end t5-->
    <%@include file="../base/_page.jsp" %>
    <!-- 分页 -->
    <div class="d">
        <div class="h">  
          <h2> 快速回复</h2>
        </div>
        <div id="reply_content">
        <div id="replyl" class="left">
        	<div class="wy_left"><p class="b lic">itll</p>
            <div class="lic">
            <img class="wy_face"  src="../images/none.gif"/>
            </div>
            </div>
            
        </div>
        <!--end postl-->
        <form id="" method="post" action="">
        <div id="replyr" class="right">
        	<div class="reply_top cc">
            <div class="reply_title pr">
            <input id="" title="请输入标题"/>
            </div>
        
            </div>
            <!--end postr_top-->
            <div class="reply_endit cc">
            <FCK:editor instanceName="FckEditor" inputName="EditorContent" toolbarSet="Reply" height="240px" width="977" basePath="/common/fckeditor/" >
				
			</FCK:editor>
            </div>
        <div class="reply_down">
        <input class="fpbtn" type="submit" id="" value="提交"/>
        </div>
        </div>
        </form>
        </div>
      </div>
      <!-- 回复 -->
   
     <!--end main-->
  
  <div id="footer"> 
  	<div >
    	<div class=" bottom cc ">
        <ul class="lic">
            <li><a href="#">清除Cookies</a></li>
            <li><a target="_blank" href="#">关于我们</a></li>
            <li><a target="_blank" href="#">管理团队</a></li>
            <li><a target="_blank" href="#">广告服务</a></li>
            <li><a href="#">无图版</a></li>
            <li><a href="#">手机浏览</a></li>
            <li><a href="#">提建议</a></li>
            <li><a href="javascript:scroll(0,0)">返回顶部</a></li>
		</ul>
        </div>
       <center  class="f9 gray" >
       <span>Total 0.008953(s) query 3, Time now is:11-04 21:41, Gzip disabled </span><br/>
       Powered by 
       <a href="#"><b>ANDRORD</b></a>
       </center>
    </div>
  </div>
  <!--end footer--> 
 </div>
</body>
</html>
