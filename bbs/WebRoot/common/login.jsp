<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
   <title>用户登录</title>

	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link rel="stylesheet" type="text/css" href="../css/base.css">
	<script src="../js/jquery-1.4.3.min.js" type="text/javascript"></script>
<script src="../js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript">
$().ready(function() {
	// validate signup form on keyup and submit
	$("#memberlogin").validate({
		rules: {
			username: {
				required: true,
				minlength: 3
			},
			password: {
				required: true,
				minlength: 5
			}
		},
		messages: {
			username: {
				required: "请输入用户名",
				minlength: "用户名至少3个字符"
			},
			password: {
				required: "请输入密码",
				minlength: "密码至少5个字符"
			}
		}
	});
	
});
</script>
  </head>
  
  <body>
<div id="page">
   <%@include file="../base/_header.jsp" %>
  <!-- top, header, index_info  -->
  <div class="clearThis"></div>
  <div id="main">

    
    <div id="location" class="cc"><img align="absmiddle"  src="../images/home.gif"/> <a title="PHP100中文网论坛" href="#">PHP100中文网论坛</a> &raquo;<span class="tr_rep">登录</span></div>
    <!--end location-->
    <div id="content">
      <div class="d">
        <div class="h"> <a class="right"> <img src="../images/cate_fold.gif" /> </a> &raquo;
          <h2> 登录 </h2>
        </div>
        <form id="memberlogin" class="cmxform" method="post" action="<%=request.getContextPath() %>/member/member_login">
        <div class="cc regItem p10">
        <dl class="cc">
        <dt>用户名</dt>
        <dd><input class="input" type="text" name="login.loginname" /><a href="#">马上注册</a></dd>
        </dl>
        <dl class="cc">
        <dt>密&nbsp;&nbsp;码</dt>
        <dd><input class="input" type="password" name="login.loginpwd" />
        <a href="#">找回密码</a>
        </dd>
        </dl>
         <dl class="cc">
        <dt>&nbsp;</dt>
        <dd><input class="btn" tabindex="10" type="submit" value="登录" />
        </dd>
        </dl>
        </div>
        </form>
      </div>
      
      
      <!--end content-->
    </div>
  </div>
  <!--end main-->
  
<%@include file="../base/_footer.jsp" %>
  <!--end footer--> 
  
</div>
</body>
</html>
